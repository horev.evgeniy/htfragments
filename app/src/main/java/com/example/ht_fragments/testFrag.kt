package com.example.ht_fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

class TestFrag : Fragment() {
    private var clickListener: TestFragClickListener? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.test_frag, container, false)
        val frag1Text = view.findViewById<TextView>(R.id.Frag1)
        frag1Text.text = arguments?.getString("openWithText")
        view.findViewById<Button>(R.id.btn_frag).apply { setOnClickListener{clickListener?.onClick()} }
        return view
    }

    companion object{
        fun newInstance(txt: String): TestFrag{
            val args = Bundle()
            args.putString("openWithText", txt)
            val fragment = TestFrag()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is TestFragClickListener){
            clickListener = context
        }
    }
    }

