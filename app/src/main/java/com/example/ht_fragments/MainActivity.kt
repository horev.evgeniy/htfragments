package com.example.ht_fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity(), TestFragClickListener  {
    private var screen : Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().add(R.id.frag_container, TestFrag.newInstance("first frag")).commit()

    }

    override fun onClick() {
        screen++
        supportFragmentManager.beginTransaction().addToBackStack(null).replace(R.id.frag_container, TestFrag.newInstance("next frag $screen")).commit()
    }
}